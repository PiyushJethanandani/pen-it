<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::where('email', 'himanshu-studylinkclasses.com')->get()->first();
        if(!$user){
            \App\User::create([
                'name'=>'Himanshu Thakkar',
                'email'=>'himanshu-studylinkclasses@gmail.com',
                'password'=>\Illuminate\Support\Facades\Hash::make('12345678'),
                'role'=>'admin'
            ]);
        }else{
            $user->update(['role'=>'admin']);
        }

        \App\User::create([
            'name'=>'Vidhi Parikh',
            'email'=>'vidhi-studylinkclasses@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('12345678')
        ]);
        \App\User::create([
            'name'=>'Yukta Peswani',
            'email'=>'yukta-studylinkclasses@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('12345678')
        ]);
    }
}
