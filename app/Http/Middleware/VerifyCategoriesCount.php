<?php

namespace App\Http\Middleware;

use App\Category;
use Closure;

class VerifyCategoriesCount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Category::count() === 0){
            session()->flash('error', 'Minimum one Category mus exists to add post!');
            return redirect(route('categories.create'));
        }
        return $next($request);
        //after this next is to register the middleware
    }
}
