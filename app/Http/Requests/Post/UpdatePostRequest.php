<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|unique:posts,title,'. $this->post->id,
            'category_id'=>'required|exists:categories,id',
            'excerpt'=>'required|max:255',
            'content'=>'required',
            'image'=>'image|mimes:jpeg,jpg,png,gif,svg|max:1024',
            'tags'=>'exists:tags,id'
        ];
    }
}
