@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">
            <div class="float-left">
                <p class="m-0">Add Post</p>
            </div>
            <div class="float-right">
                <a href="" data-toggle="modal" data-target="#previewModal" id="preview">Live Preview</a>
            </div>
        </div>

        <div class="card-body">
            <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text"
                           value="{{ old('title') }}"
                           class="form-control @error('title') is-invalid @enderror"
                           name="title" id="title">
                    @error('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="category_id">Select Category</label>
                    <select name="category_id" id="category_id" class="form-control">
                        <option value="0" selected>Select Category</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tags[]">Select Tags</label>
                    <select name="tags[]" id="tags" class="form-control" multiple>
                        @foreach($tags as $tag)
                            <option value="{{$tag->id}}">{{$tag->name}}</option>
                        @endforeach
                    </select>
                    @error('tags')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="excerpt">Excerpt</label>
                    <textarea name="excerpt"
                              id="excerpt"
                              class="form-control @error('excerpt') is-invalid @enderror"
                              rows="4">{{ old('excerpt') }}</textarea>
                    @error('excerpt')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <input type="hidden" name="content" id="content">
                    <trix-editor input="content"></trix-editor>
                    @error('content')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="published_at">Published At</label>
                    <input type="date"
                           value="{{ old('published_at') }}"
                           class="form-control"
                           name="published_at" id="published_at">
                    @error('published_at')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file"
                           value="{{ old('image') }}"
                           class="form-control @error('image') is-invalid @enderror"
                           name="image" id="image">
                    @error('image')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Add Post</button>
                </div>
            </form>
        </div>
    </div>
    <img src="" width="200" style="display:none;" />

    <!-- DELETE MODAL -->
    <div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Blog Preview</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="blog-three-mini">
                        <h2 class="color-dark" id="preview-title"></h2>
                        <div class="blog-three-attrib">
                            <div><i class="fa fa-calendar"></i><p id="preview-date"></p></div> |
                            <div><i class="fa fa-pencil"></i><a href="#"><span id="preview-author">Mihir</span></a></div> |
                            <div>
                                Share:  <a href="#"><i class="fa fa-facebook-official"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </div>
            
                        <img alt="Blog Image" class="img-responsive" width="100%" id="preview-image">
                        <div class="lead mt25" id="preview-lead">
                        </div>
            
                        <div class="blog-post-read-tag mt50" id="preview-tags">
                            <i class="fa fa-tags"></i> Tags: <span class="preview-tags"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /DELETE MODAL -->

@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        flatpickr("#published_at", {
            enableTime: true
        });
        $(document).ready(function() {
            $('#category_id').select2();
        });
        $(document).ready(function() {
            $('#tags').select2();
        });
        $('#preview').click(function(e){
            document.getElementById("preview-title").innerHTML = $("#title").val();
            document.getElementById("preview-lead").innerHTML = $('#content').val();
            $('#tags').select2('data').forEach((element, index, array)=> {
                console.log(index === (array.index-1));
                
                if(index == (array.index-1))
                    document.getElementById('preview-tags').innerHTML += element.text +  "";
                else
                    document.getElementById('preview-tags').innerHTML += element.text +  ", ";
            });
        });
        $('#image').change( function(event) {
            console.log(event.target.files[0]);
            $("#preview-image").attr('src',URL.createObjectURL(event.target.files[0]));
        });
    </script>
@endsection
@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
   <link rel="stylesheet" href="{{asset('assets/css/main/main.css')}}">
   <link rel="stylesheet" href="{{asset('assets/css/main/setting.css')}}">
   <link rel="stylesheet" href="{{asset('assets/css/main/hover.css')}}">


   <link rel="stylesheet" href="{{asset('assets/css/color/pasific.css')}}">
   <link rel="stylesheet" href="{{asset('assets/css/icon/font-awesome.css')}}">
   <link rel="stylesheet" href="{{asset('assets/css/icon/et-line-font.css')}}">
@endsection
