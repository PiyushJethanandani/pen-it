<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'WelcomeController@index')->name('welcome');
Route::get('/blog/{post}', "WelcomeController@show")->name('blog.show');
Route::get('/blog/category/{category}', 'WelcomeController@category')->name('welcome.category');
Route::get('/blog/tag/{tag}', 'WelcomeController@tag')->name('welcome.tag');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//This will create routes for 7 default actions automatically.

Route::middleware(['auth'])->group(function(){
    Route::resource('categories', 'CategoriesController');
    Route::resource('posts', 'PostsController');
    Route::resource('tags', 'TagsController');
    Route::delete('/trash/{post}', "PostsController@trash")->name('posts.trash');
    Route::get('/trashed', "PostsController@trashed")->name('posts.trashed');
    Route::put('/restore/{post}', "PostsController@restore")->name('posts.restore');
});
Route::middleware(['auth', 'admin'])->group(function (){
    Route::get('/users', 'UsersController@index')->name('users.index');
    Route::put('/users/{user}/make-admin', 'UsersController@makeAdmin')->name('users.make-admin');
});
